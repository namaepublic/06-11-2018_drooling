﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public static class Helpers
{
#if UNITY_EDITOR
    public static IEnumerable<T> FindAssetsByType<T>() where T : Object
    {
        return AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T))).Select(AssetDatabase.GUIDToAssetPath)
            .Select(AssetDatabase.LoadAssetAtPath<T>).Where(asset => asset != null);
    }

    public static List<T> GetComponentsInChildrenWithTag<T>(this Component component, string tag, bool includeInactive = false)
        where T : Component
    {
        return component.GetComponentsInChildren<T>(includeInactive).Where(item => item.CompareTag(tag)).ToList();
    }

    public static T GetComponentInChildrenWithTag<T>(this Component component, string tag, bool includeInactive = false)
        where T : Component
    {
        return component.GetComponentsInChildren<T>(includeInactive).FirstOrDefault(item => item.CompareTag(tag));
    }
#endif
}