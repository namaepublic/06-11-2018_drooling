﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private LevelsController _levelsController;
    [SerializeField] private GameObject _menuCanvas, _guiCanvas;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _levelsController = FindObjectOfType<LevelsController>();
    }
#endif

    private void Start()
    {
        Init();
    }

    public void Init()
    {
        _guiCanvas.SetActive(false);
        _menuCanvas.SetActive(true);
        _levelsController.Init();
    }

    public void StartGame()
    {
        _menuCanvas.SetActive(false);
        _guiCanvas.SetActive(true);
        NextLevel();
    }

    public void NextLevel()
    {
        if (!_levelsController.NextLevel())
            GameOver();
    }

    public void RestartLevel()
    {
        _levelsController.RestartLevel();
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}