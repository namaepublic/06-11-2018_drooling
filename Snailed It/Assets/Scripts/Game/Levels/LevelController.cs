﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class LevelController : MonoBehaviour
{
    [SerializeField] private Transform _startPosition;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private FaceRotation _faceRotation;

#if UNITY_EDITOR
    public Transform StartPosition
    {
        get { return _startPosition; }
    }

    public FaceRotation FaceRotation
    {
        get { return _faceRotation; }
    }

    private void OnValidate()
    {
        _startPosition = this.GetComponentInChildrenWithTag<Transform>("StartPosition");
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (!EditorApplication.isPlaying && _playerController != null)
            _playerController.Init(_startPosition.position, _faceRotation);
    }
#endif

    public void Init()
    {
        gameObject.SetActive(false);
        _playerController.ManualReset();
    }

    public void StartLevel()
    {
        gameObject.SetActive(true);
        _playerController.Init(_startPosition.position, _faceRotation);
    }
}