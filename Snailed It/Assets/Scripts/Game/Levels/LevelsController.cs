﻿using System.Linq;
using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private LevelController[] _levelControllers;

    private int _level;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _levelControllers = GetComponentsInChildren<LevelController>(true).ToArray();
    }
#endif

    public void Init()
    {
        _level = 0;
        foreach (var levelController in _levelControllers)
            levelController.Init();
    }

    public void RestartLevel()
    {
        _level--;
        NextLevel();
    }

    public bool NextLevel()
    {
        if (_level >= _levelControllers.Length) return false;
        foreach (var levelController in _levelControllers)
            levelController.Init();
        
        _levelControllers[_level++].StartLevel();
        return true;
    }
}