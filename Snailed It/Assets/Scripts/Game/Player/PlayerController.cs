﻿using System;
using System.Linq;
using UnityEngine;

public enum FaceRotation
{
    RIGHT,
    LEFT,
    UP,
    DOWN
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    private enum MovementIntent
    {
        NONE,
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    [SerializeField] private GameController _gameController;
    [SerializeField] private Transform _playerSprite;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private float _force;
    [SerializeField] private ForceMode2D _forceMode;
    [SerializeField] private float _stopPrecision = .01f;
    [SerializeField] private float _recenterPrecision = .01f;
    [SerializeField] private Vector3 _faceRight, _faceLeft, _faceUp, _faceDown;
    [SerializeField] [HideInInspector] private int _wallLayer, _goalLayer, _holeLayer;

    private MovementIntent _intent;
    private bool _isMoving;
    private bool _isInit;

#if UNITY_EDITOR
    [SerializeField] private LevelController _levelController;

    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _levelController = FindObjectOfType<LevelController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _wallLayer = LayerMask.NameToLayer("Wall");
        _goalLayer = LayerMask.NameToLayer("Goal");
        _holeLayer = LayerMask.NameToLayer("Hole");

        if (_levelController != null)
            Init(_levelController.StartPosition.position, _levelController.FaceRotation);
    }
#endif

    public void ManualReset()
    {
        _isInit = false;
        _rigidbody2D.velocity = Vector2.zero;
    }

    public void Init(Vector3 position, FaceRotation faceRotation)
    {
        _isInit = true;
        transform.position = position;
        switch (faceRotation)
        {
            case FaceRotation.RIGHT:
                _playerSprite.transform.rotation = Quaternion.Euler(_faceRight);
                break;
            case FaceRotation.LEFT:
                _playerSprite.transform.rotation = Quaternion.Euler(_faceLeft);
                break;
            case FaceRotation.UP:
                _playerSprite.transform.rotation = Quaternion.Euler(_faceUp);
                break;
            case FaceRotation.DOWN:
                _playerSprite.transform.rotation = Quaternion.Euler(_faceDown);
                break;
        }
    }

    private void Update()
    {
        if (!_isInit) return;

        _isMoving = _rigidbody2D.velocity.magnitude > _stopPrecision;

        _intent = MovementIntent.NONE;
        GetMovement(MovementIntent.UP, KeyCode.Z, KeyCode.UpArrow);
        GetMovement(MovementIntent.DOWN, KeyCode.S, KeyCode.DownArrow);
        GetMovement(MovementIntent.LEFT, KeyCode.Q, KeyCode.LeftArrow);
        GetMovement(MovementIntent.RIGHT, KeyCode.D, KeyCode.RightArrow);
    }

    private void FixedUpdate()
    {
        if (!_isInit) return;

        SetMovement(MovementIntent.UP, Vector2.up, _faceUp);
        SetMovement(MovementIntent.DOWN, Vector2.down, _faceDown);
        SetMovement(MovementIntent.LEFT, Vector2.left, _faceLeft);
        SetMovement(MovementIntent.RIGHT, Vector2.right, _faceRight);
    }

    private void GetMovement(MovementIntent intent, params KeyCode[] keyCodes)
    {
        if (_isMoving || !keyCodes.Aggregate(false, (current, keyCode) => current || Input.GetKeyDown(keyCode))) return;

        _intent = intent;
    }

    private void SetMovement(MovementIntent intent, Vector2 direction, Vector3 faceRotation)
    {
        if (_isMoving || _intent != intent) return;
        _playerSprite.transform.rotation = Quaternion.Euler(faceRotation);
        _rigidbody2D.AddForce(direction * _force * Time.deltaTime, _forceMode);
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (!_isInit || other.gameObject.layer != _wallLayer) return;

        foreach (var contact in other.contacts)
            transform.Translate(contact.normal * _recenterPrecision);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_isInit) return;
        if (other.gameObject.layer == _goalLayer)
            _gameController.NextLevel();
        else if (other.gameObject.layer == _holeLayer)
            _gameController.RestartLevel();
    }
}